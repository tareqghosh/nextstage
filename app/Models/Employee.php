<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function company(){
        return $this->belongsTo('App\Models\Company', 'company_id');
    }
}
