<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'company_id' => 'required|exists:companies,id',
            'phone' => 'required|unique:employees,phone',
            'email' => 'required|unique:employees,email'
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $rules['phone'] = 'required|unique:employees,phone,' . request()->route('employee');
            $rules['email'] = 'required|unique:employees,email,' . request()->route('employee');
        }

        return $rules;
    }
}
