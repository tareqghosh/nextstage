<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'url' => 'required|string|max:255',
            'logo' => 'required|file|mimes:png,jpg'
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $rules['name'] = 'nullable|file|mimes:png,jpg';
        }
        return $rules;
    }
}
