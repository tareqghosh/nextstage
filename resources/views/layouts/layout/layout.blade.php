@include('layouts/layout/header')

<head>
    @yield('head')
    <style>
        .dropdown-menu.dropdown-menu-fit {
            border-radius: 10px;
            overflow: hidden;
        }

        .kt-header__topbar-item--user.show .fa-bell {
            color: #494b74;
        }

        .kt-aside__brand-logo img {
            width: 55px;
        }

        .bell-icon button {
            background-color: transparent;
            border: 0;
            color: #aaa;
            font-size: 30px;
            position: relative;
        }

        .bell-icon button.new::after {
            content: "";
            position: absolute;
            top: -2px;
            right: 1px;
            width: 15px;
            height: 15px;
            background-color: red;
            border-radius: 50%;
            box-shadow: 0px 0px 5px 2px #ff000085;
        }

        .notifications-list {
            padding: 0;
            margin: 0;
            list-style: none;
            width: 100%;
            overflow: auto;
            max-height: 300px;
        }

        .notifications-list li {
            border-bottom: 1px solid #efefef;
        }

        .notifications-list li a {
            width: 100%;
            padding: 13px 10px;
        }

        .notifications-list .notification-title {
            font-size: 15px;
            font-weight: 500;
            color: #555;
        }

        .notifications-list .notification-time {
            color: #2dc0d5;
        }

        .notifications-list li a:hover {
            background-color: #efefef;
        }

        .view-all-notifications {
            font-weight: 600;
        }

        .view-all-notifications:hover {
            color: #494b74;
        }


        /** edit pagination **/
        .page-item .page-link {
            border: 0;
            margin: 0 3px;
            padding: 5px 12px;
            border-radius: 3px;
            color: #93a2dd;
            font-size: 15px;
            font-weight: 500;
        }

        .page-item.disabled .page-link {
            color: #aaa;
        }

        .page-item .page-link:hover {
            background-color: #5d78ff;
            color: #fff;
        }

        .page-item.active .page-link {
            background-color: #5d78ff;
        }

        li.page-item:first-child .page-link,
        li.page-item:last-child .page-link {
            font-size: 34px;
            line-height: 14px;
            padding-bottom: 8px;
            background-color: #f0f3ff;
        }

        li.page-item:first-child .page-link:hover,
        li.page-item:last-child .page-link:hover {
            background-color: #5d78ff;
        }

        /** end edit pagination **/
        .cursor-pointer {
            cursor: pointer;
        }

        .kt-datatable>.kt-datatable__table>.kt-datatable__head .kt-datatable__row>.kt-datatable__cell>span.active {
            color: #5d78ff;
            font-weight: bold;
        }

        .kt-avatar .kt-avatar__holder {
            background-position: center;
        }

        .price-input {
            position: relative;
        }

        .price-input::before {
            content: "JD";
            position: absolute;
            top: 10px;
            right: 35px;
        }

        .tax-input {
            position: relative;
        }

        .tax-input::before {
            content: "%";
            position: absolute;
            top: 10px;
            right: 35px;
        }

        /* file upload drag and drop */
        .profile-img-holder {
            background-size: cover;
            width:140px;
            height: 140px;
            border: 1px solid #ccc;
            background-position: center;
            margin: auto;
            margin-bottom: 10px;
        }

        .drag-area:hover,
        .hover {
            border: 4px dashed rgb(29, 90, 139);
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
    @include('layouts/layout/sidebar')
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        <!-- begin:: Header -->
        <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
            <div class="col-md-4">
                <a class="back-button btn" id="goback" role="button"><i class="fa fa-chevron-left"
                        aria-hidden="true"></i>Back</a>
            </div>
            <div class="kt-header__topbar">
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper row" data-offset="30px,0px" aria-expanded="true">
                    </div>
                </div>
                <!--end: Quick Actions -->
                <!--begin: Notifications -->
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px"
                        aria-expanded="true">
                        <span class="kt-header__topbar-icon kt-pulse kt-pulse--brand">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <path
                                        d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z"
                                        fill="#000000" />
                                    <rect fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4" rx="2" />
                                </g>
                            </svg> <span class="kt-pulse__ring"></span>
                        </span>
                    </div>
                </div>
                <!--end: Notifications -->
                <!--begin: My Cart -->
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                        <div class="kt-header__topbar-user">
                            <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                            <span class="kt-header__topbar-username kt-hidden-mobile">{{auth()->user()->name}}</span>
                            <img class="kt-hidden" alt="Pic" src="/assets/media/users/300_25.jpg" />
                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span
                                class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{isset(Auth()->user()->name[0]) ? Auth()->user()->name[0] : ''}}</span>
                        </div>
                    </div>
                    <div
                        class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                        <!--end: Head -->
                        <!--begin: Navigation -->
                        <div class="kt-notification">
                            <div class="kt-notification__custom kt-space-between">
                                <form action="{{route('logout')}}" method="POST" style="margin: auto;">
                                    @csrf
                                    <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold">
                                        <i class="fa fa-sign-out-alt"></i> Log out
                                    </button>
                                </form>
                            </div>
                        </div>
                        <!--end: Navigation -->
                    </div>
                </div>

                <!--end: User Bar -->
            </div>
            <!-- end:: Header Topbar -->
        </div>
        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
            <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-subheader__main">
                        @yield('body-header')
                    </div>
                </div>
            </div>
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        <div class="alert-icon"><i class="flaticon-success"></i></div>
                        <div class="alert-text">{{session("success")}}</div>
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger" role="alert">
                        <div class="alert-icon"><i class="flaticon-danger"></i></div>
                        <div class="alert-text">{{session("error")}}</div>
                    </div>
                    @endif
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @yield('body')
                </div>
            </div>
        </div>
        <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-footer__copyright">
                    2020&nbsp;&copy;&nbsp;<a target="_blank" class="kt-link">Al-Junadi All Rights Reserved</a>
                </div>
            </div>
        </div>
    </div>
    @include('layouts/layout/scripts')
    @yield('scripts')
</body>
