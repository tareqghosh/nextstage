		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": [
							"#c5cbe3",
							"#a1a8c3",
							"#3d4465",
							"#3e4466"
						],
						"shape": [
							"#f0f3ff",
							"#d9dffa",
							"#afb4d4",
							"#646c9a"
						]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="/assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="/assets/js/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="/assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>

		<!--end::Page Vendors -->
		<script src="/assets/js/pages/components/extended/sweetalert2.js" type="text/javascript"></script>

		<!--begin::Page Scripts(used by this page) -->
        <script src="/assets/js/pages/dashboard.js" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/bootstrap-datepicker.init.js"></script>
        <script src="/js/select2.js"></script>
        <script src="/js/main.js"></script>
        <script>
            $("#goback").click(function (){
  window.history.back();
});
            $(".kt-datatable__cell").on('click', function(){
                $(this).closest('dropdown').find('.dropdown').addClass('show');
                $(this).closest('dropdown-menu-right').find('.dropdown').addClass('show');
            });
                  $('#checkAll').on('change', function(){
          if(this.checked){
            $('input[name^="mark[]"]:checkbox').each(function(){
            this.checked = true;
});
          } else {
            $('input[name^="mark[]"]:checkbox').each(function(){
            this.checked = false;
});
          }

      });


  /**** rate scripts ****/
  jQuery(document).ready(function(){
    $('.feedback-btn').click(function(){
            $('.rate-radio').attr('checked', false);
            if($(this).hasClass('happy')) {
                $('#happy').attr('checked', true);
                $('.feedback-btn').removeClass('active');
                $(this).addClass('active');
            }
            else if($(this).hasClass('neutral')) {
                $('#neutral').attr('checked', true);
                $('.feedback-btn').removeClass('active');
                $(this).addClass('active');
            }
            else {
                $('#unhappy').attr('checked', true);
                $('.feedback-btn').removeClass('active');
                $(this).addClass('active');
            }
        });
  });

      </script>
		<!--end::Page Scripts -->
