@extends('layouts.layout.layout')
@section('head')
<title>Employees</title>
@endsection
@section('body-header')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Employees
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{$employees->total()}} Total </span>
            </div>
        </div>
    </div>
</div>
@endsection

@section('body')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="text-right mb-3">
        <a class="btn btn-brand btn-elevate" href="{{route('employee.create')}}"><i class="fa fa-plus mx-2"></i>Create a new employee</a>
    </div>
    <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="">
                    <table class="kt-datatable__table" style="display: block;">
                        <thead class="kt-datatable__head">
                            <tr class="kt-datatable__row" style="left: 0px;">
                                <th class="kt-datatable__cell"><span style="width: 100px;">Employee ID</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Employee name</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Employee phone</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Employee email</span></th>
                                <th class="kt-datatable__cell"><span style="width: 100px;">Employee company</span></th>
                                <th class="kt-datatable__cell"><span style="width: 70px;">Actions</span></th>
                            </tr>
                        </thead>
                        <tbody style="" class="kt-datatable__body">
                            @foreach($employees as $key=>$company)
                            <tr data-row="{{$company->id}}" class="kt-datatable__row">
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$company->id}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$company->name}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$company->phone}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{$company->email}}</span>
                                </td>
                                <td class="kt-datatable__cell--sorted kt-datatable__cell" data-field="AgentName">
                                    <span style="width: 100px;">{{@$company->company->name}}</span>
                                </td>
                                <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell"><span style="overflow: visible; position: relative; width: 70px;">
                                        <div class="dropdown middle"> <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="flaticon-more-1"></i> </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <ul class="kt-nav">
                                                    <li class="kt-nav__item"> <a class="kt-nav__link" href="{{route('employee.edit', $company->id)}}"> <i class="kt-nav__link-icon flaticon2-contract"></i> <span class="kt-nav__link-text">Edit Employee</span> </a> </li>
                                                    <form action="{{route('employee.destroy', $company->id)}}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                    <li class="kt-nav__item"> <button type="submit" class="btn btn-danger btn-sm rounded-0" style="margin-left: 10px;"> <i class="fa fa-trash"></i> <span class="kt-nav__link-text">Delete Employee</span> </button> </li>
                                                    </form>
                                                </ul>
                                            </div>
                                        </div>
                                    </span></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="kt-pagination kt-pagination--brand">
                {{$employees->links()}}
            </div>
    </div>
</div>
@endsection
