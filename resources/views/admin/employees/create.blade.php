@extends('layouts/layout/layout')

@section('head')
<title>Add an employee</title>
@endsection

@section('body')
	<!--begin::Portlet-->
    <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Add an employee
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{route('employee.store')}}">
                @csrf
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>First name</label>
                        <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" required>
                        @if ($errors->has('first_name'))
                        <span class="help-block" style="color: red">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Last name</label>
                        <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" required>
                        @if ($errors->has('last_name'))
                        <span class="help-block" style="color: red">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="tel" class="form-control" name="phone" value="{{old('phone')}}" required>
                        @if ($errors->has('phone'))
                        <span class="help-block" style="color: red">{{ $errors->first('phone') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{old('email')}}" required>
                        @if ($errors->has('email'))
                        <span class="help-block" style="color: red">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Company</label>
                        <select name="company_id" required class="form-control">
                            @foreach($companies as $company)
                            <option value="{{$company->id}}" {{old('company_id') == $company->id ? 'selected' : ''}}> {{$company->name}} </option>
                            @endforeach
                        </select>
                        @if ($errors->has('company_id'))
                        <span class="help-block" style="color: red">{{ $errors->first('company_id') }}</span>
                        @endif
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

@stop
@section('scripts')
<script>
</script>
@stop
