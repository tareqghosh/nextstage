@extends('layouts/layout/layout')

@section('head')
<title>Create a company</title>
@endsection

@section('body')
	<!--begin::Portlet-->
    <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Create a company
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{route('company.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
                        @if ($errors->has('name'))
                        <span class="help-block" style="color: red">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" value="{{old('email')}}" required>
                        @if ($errors->has('email'))
                        <span class="help-block" style="color: red">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>url</label>
                        <input type="url" class="form-control" name="url" value="{{old('name')}}" required>
                        @if ($errors->has('url'))
                        <span class="help-block" style="color: red">{{ $errors->first('url') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" class="form-control" name="logo" required>
                        @if ($errors->has('logo'))
                        <span class="help-block" style="color: red">{{ $errors->first('logo') }}</span>
                        @endif
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

@stop
@section('scripts')
<script>
</script>
@stop
