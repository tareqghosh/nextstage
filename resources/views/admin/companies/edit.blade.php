@extends('layouts/layout/layout')

@section('head')
<title>Edit a company</title>
@endsection

@section('body')
	<!--begin::Portlet-->
    <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit a company
                    </h3>
                </div>
            </div>

            <!--begin::Form-->
            <form class="kt-form" method="POST" action="{{route('company.update', $company->id)}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="kt-portlet__body">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control" name="name" value="{{$company->name}}" required>
                        @if ($errors->has('name'))
                        <span class="help-block" style="color: red">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" value="{{$company->email}}" required>
                        @if ($errors->has('email'))
                        <span class="help-block" style="color: red">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>url</label>
                        <input type="url" class="form-control" name="url" value="{{$company->url}}" required>
                        @if ($errors->has('url'))
                        <span class="help-block" style="color: red">{{ $errors->first('url') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" class="form-control" name="logo">
                        @if ($errors->has('logo'))
                        <span class="help-block" style="color: red">{{ $errors->first('logo') }}</span>
                        @endif
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

@stop
@section('scripts')
<script>
</script>
@stop
